const form= document.getElementById("transactionForm");
const inputs = document.querySelectorAll('#transactionForm input');
const warning = document.getElementById('message');
const regularExpressions = {
    number : /([0-9])/,
    text : /([a-zA-Z])/,
    character : /^[^a-zA-Z\d\s]+$/,
    email : /^([a-z\d]+[@]+[a-z]+\.[a-z]{2,})+$/,
    espace : /\s/g
}

inputs.forEach(input=>{
    input.addEventListener('keyup', (event) => {   
        let valueInput = event.target.value;
        switch(event.target.id){
            case 'transactionCode':
                
                if(event.target.value.length == 5){
                    event.target.style.border ='5px solid green';
                    warning.innerHTML = '';
                }else{
                    event.target.style.border = '5px solid red';
                    warning.innerHTML = 'Debe ingresar un codigo de 5 caracteres máximo';
                }
            break;
            case 'transactionTitle':
                
                if((event.target.value.length >= 20) && (event.target.value.length <=100)){
                    event.target.style.border ='5px solid green';
                    warning.innerHTML = '';
                }else{
                    event.target.style.border = '5px solid red';
                    warning.innerHTML = 'Debe ingresar un titulo de entre 10 y 100 caracteres.';
                }
            break;
            case 'transactionAuthor':
                
                if((event.target.value.length >= 10) && (event.target.value.length <=60)){
                    event.target.style.border ='5px solid green';
                    warning.innerHTML = '';
                }else{
                    event.target.style.border = '5px solid red';
                    warning.innerHTML = 'Debe ingresar un autor de entre 10 y 60 caracteres.';
                }
            break;
            case 'transactionEditorial':
                
                if((event.target.value.length >= 10) && (event.target.value.length <=30)){
                    event.target.style.border ='5px solid green';
                    warning.innerHTML = '';
                }else{
                    event.target.style.border = '5px solid red';
                    warning.innerHTML = 'Debe ingresar una editorial de entre 10 y 30 caracteres.';
                }
            break;
        }
    })
});

form.addEventListener("submit", event=>{
    event.preventDefault();
    var transactionCode = document.getElementById('transactionCode');
    var transactionTitle = document.getElementById('transactionTitle');
    var transactionAuthor = document.getElementById('transactionAuthor');
    var transactionEditorial = document.getElementById('transactionEditorial');
    var transactionDatePublic = document.getElementById('transactionDatePublic');
    var transactionEntry = document.getElementById('transactionEntry');

    if(transactionCode.value==""){
        warning.innerHTML = "DEBE LLENAR LOS CAMPOS";
    }else if(transactionTitle.value== ""){
        warning.innerHTML = "DEBE LLENAR LOS CAMPOS";
    }else if(transactionAuthor.value==""){
        warning.innerHTML = "DEBE LLENAR LOS CAMPOS";
    }else if(transactionEditorial.value==""){
        warning.innerHTML = "DEBE LLENAR LOS CAMPOS";
    }else if(transactionDatePublic.value==""){
        warning.innerHTML = "DEBE LLENAR LOS CAMPOS";
    }else if(transactionEntry.value==""){
        warning.innerHTML = "DEBE LLENAR LOS CAMPOS";
    }else if (transactionEntry.value < transactionDatePublic.value) {
        alert("La fecha de ingreso debe ser mayor a la fecha de publicación puesta");
        return false;
      }
    else{
        warning.innerHTML = '';
        alert("¡DATOS SE ENVIARON, TENGA BUEN DIA!");
    }
      
})
